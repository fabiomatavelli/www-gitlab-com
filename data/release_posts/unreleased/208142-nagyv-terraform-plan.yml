features:
  primary:
    - name: "Review Terraform plan output from the Merge Request"
      available_in: [core, starter, premium, ultimate]
      gitlab_com: false
      documentation_link: 'https://docs.gitlab.com/ee/user/infrastructure/'
      image_url: '/images/unreleased/terraform-plan-in-mr.png'
      reporter: nagyv-gitlab
      stage: configure
      categories:
        - 'Infrastructure as Code'
      issue_url: 'https://gitlab.com/groups/gitlab-org/-/epics/2676'
      description: |
        In GitLab 13.0, GitLab CI/CD now displays the summary of the `terraform plan` command directly in your merge request, instead of in job logs. The `terraform plan` command helps you verify proposed infrastructure changes have the intended effect, by showing the changes to be applied to your infrastructure base on the code changes. Merge requests now provide a summary of changes and a shortcut button to display the full job log, while previous GitLab versions required a time-consuming manual process to get to the results of the command.

        Users of the [Terraform template provided by GitLab](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml) will see the `terraform plan` merge request widget without additional configuration. Users of customized CI/CD templates for Terraform can update their template to use the image and scripts in the [official GitLab Terraform template](https://gitlab.com/gitlab-org/gitlab/-/blob/master/lib/gitlab/ci/templates/Terraform.gitlab-ci.yml). 
