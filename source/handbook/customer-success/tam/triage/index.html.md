---
layout: handbook-page-toc
title: "Customer Triage Process"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

----

**All triage information has been consolidated to the [Customer Health Assessment and Triage](/handbook/customer-success/tam/health-score-triage/) page.**