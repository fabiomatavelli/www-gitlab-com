---
layout: handbook-page-toc
title: Customer Console
category: GitLab.com
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Overview

Using the customer console for internal requests is only for specials cases where the existing tools won't allow us complete the task at hand.
To access the console a previously completed [Access Request](https://gitlab.com/gitlab-com/access-requests/issues/new?issuable_template=Single%20Person%20Access%20Request) ticket must exist which depends on the completing the [Console Bootcamp](https://gitlab.com/gitlab-com/support/support-training/-/issues/new?issuable_template=Bootcamp%20-%20GitLab.com%20Console) and requisities.



## Using the support console

After logging into the customer portal server, enter the command:

```
$ support-console
```

This will open the console and automatically load the functions available to use.

Most functions rely on the namespace (i.e. GitLab.com Group name or username) , always make sure to have it handy before starting any work from the console.

## Scope

The console will be for taks which cannot be completed from the tools we have available. 

We need to see the console as a `transition` stage:

```mermaid
graph TD
  A[Console Tasks] --> B(Automate within GitLab)
  B --> C(Integrate into the product)

```
The more we use a function the more we should ask ourselves why we haven't automated that process or even better integrated that missing function into our product.

## Functions for frequently requested tasks

### **change_plan**

It will modify an existing plan, by changing its type or extending his trial lenght and output the modified namespace information.

### Parameters

`:namespce` the namespace to change

`:plan` The new plan to apply

`:date` [ Optional ] The due date for the trail in the given namespace

```ruby
irb(main):001:0> change_plan('example','silver','2020-05-25')
{"id"=>0000000,
 "name"=>"example",
 "path"=>"example",
 "kind"=>"group",
 "full_path"=>"example",
 "parent_id"=>nil,
 "avatar_url"=>
  "https://gitlab.com/uploads/-/system/group/avatar/0000000/icon.png",
 "web_url"=>"https://gitlab.com/groups/example",
 "members_count_with_descendants"=>43,
 "shared_runners_minutes_limit"=>10000,
 "extra_shared_runners_minutes_limit"=>2000,
 "billable_members_count"=>44,
 "plan"=>"silver",
 "trial_ends_on"=>"2020-05-25",
 "trial"=>true}
```

This function can be used without the last parameter `date` and it will only change the plan not the due date.

### **view_namespace**

Provides a unified view for the namespace including orders and customer account linked to the orders.

```ruby
irb(main):001:0> view_namespace('testspace')

[+]Namespace information
id                                1111111
name                              test
path                              testspace
members_count_with_descendants    3
shared_runners_minutes_limit      2000
billable_members_count            3
plan                              bronze
trial_ends_on                     2020-05-20
trial                             true

[+] There are 1 orders for this namespace
 id                                33333
 customer_id                       2222222
 subscription_id
 subscription_name
 start_date                        2020-05-10
 end_date                          2020-05-20
 gl_namespace_id                   1111111
 gl_namespace_name                 testspace

[+] Customer linked to orders
 https://customers.gitlab.com/admin/customer/2222222
 id                                2222222
 company                           Test Corp
 first_name                        John
 last_name                         Doe
 email                             John Doe
 uid                               1111111
 zuora_account_id
```

## FAQ

* How can I add function ?

    - Create a MR for `support_team.rb` on the [console-training-wheels project](https://gitlab.com/gitlab-com/support/toolbox/console-training-wheels)

* Can I use other code not available in `support_team.rb`?
    - Yes, when you're confortable with ruby code and IRB, just make sure to merge the code in the library for everynoe to use it.

